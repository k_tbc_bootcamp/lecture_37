package com.example.lecture_37.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.lecture_37.CustomCallback
import com.example.lecture_37.MainActivity
import com.example.lecture_37.R
import com.example.lecture_37.models.MenuModel
import kotlinx.android.synthetic.main.menu_item_recycler_view_layout.view.*

class MenuRecyclerViewAdapter(
    private val activity: MainActivity,
    val items: MutableList<MenuModel>,
    private val customCallback: CustomCallback
) :
    RecyclerView.Adapter<MenuRecyclerViewAdapter.ViewHolder>() {

    private var currentPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.menu_item_recycler_view_layout, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var item: MenuModel

        fun onBind() {
            item = items[adapterPosition]
            itemView.iconImageView.setImageResource(item.icon)
            itemView.titleTextView.text = item.text

            if (currentPosition == adapterPosition) {
                itemView.setBackgroundColor(
                    ContextCompat.getColor(
                        activity,
                        android.R.color.darker_gray
                    )
                )
                itemView.titleTextView.setTextColor(
                    ContextCompat.getColor(
                        activity,
                        android.R.color.white
                    )
                )
            } else {
                itemView.setBackgroundColor(ContextCompat.getColor(activity, android.R.color.white))
                itemView.titleTextView.setTextColor(
                    ContextCompat.getColor(
                        activity,
                        android.R.color.black
                    )
                )
            }
            itemView.setOnClickListener {
                customCallback.onClick(adapterPosition)
                currentPosition = adapterPosition
                notifyDataSetChanged()
            }
        }
    }
}