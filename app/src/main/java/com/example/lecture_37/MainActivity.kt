package com.example.lecture_37

import android.os.Bundle
import android.view.Menu
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lecture_37.adapters.MenuRecyclerViewAdapter
import com.example.lecture_37.models.MenuModel
import com.example.lecture_37.ui.gallery.GalleryFragment
import com.example.lecture_37.ui.home.HomeFragment
import com.example.lecture_37.ui.slideshow.SlideshowFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var drawerLayout: DrawerLayout
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var menuRecyclerViewAdapter: MenuRecyclerViewAdapter
    private val fragments = mutableListOf<Fragment>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        drawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.navHostFragment)
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        initMenuItems()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.navHostFragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun initMenuItems() {
        val items = mutableListOf<MenuModel>()
        items.add(MenuModel(R.drawable.ic_menu_camera, "Camera"))
        items.add(MenuModel(R.drawable.ic_menu_gallery, "Gallery"))
        items.add(MenuModel(R.drawable.ic_menu_slideshow, "Slideshow"))
        menuRecyclerViewAdapter = MenuRecyclerViewAdapter(this, items, object : CustomCallback {
            override fun onClick(adapterPosition: Int) {
                supportFragmentManager.beginTransaction()
                    .replace(R.id.navHostFragment, fragments[adapterPosition]).commit()
                drawerLayout.closeDrawers()
            }

        })
        itemsRecyclerView.layoutManager = LinearLayoutManager(this)
        itemsRecyclerView.adapter = menuRecyclerViewAdapter

        fragments.addAll(arrayOf(HomeFragment(), GalleryFragment(), SlideshowFragment()))
    }
}
