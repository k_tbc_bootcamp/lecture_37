package com.example.lecture_37

interface CustomCallback {
    fun onClick(adapterPosition: Int)
}